#include <stdio.h>
#include <math.h>
int main (void){

    //variáveis
    float resultA;
    int x, y, resultB;

    //entrada de dados
    printf("Informe o valor de x: ");
    scanf("%d",&x);
    printf("Informe o valor de y: ");
    scanf("%d",&y);

    //processamento e saída de dados
    resultA = (float)((pow(x,2)) * (x + y)/y);
    printf("a) %f", resultA);

    resultA = (float)(x + y)/(x - y);
    printf("\nb) %f", resultA);

    resultA = (float)((pow(x,2) + pow(y,3))/2);
    printf("\nc) %f", resultA);

    resultA = (float)((pow(x,3))/(pow(x,2)));
    printf("\nd) %f", resultA);

    resultB = x % y;
    printf("\ne) %d", resultB);

    resultB = x % 3;
    printf("\nf) %d", resultB);

    resultB = y % 5;
    printf("\ng) %d", resultB);

    return 0;
}
