/* 3) Fa�a um programa que leia o g�nero, e caso o mesmo seja 'f', 'F', 'm' ou 'M' leia
tamb�m a idade e o tempo de trabalho de uma pessoa e determine se ela pode ou n�o se
aposentar. Assuma que homens se aposentam com 45 anos de trabalho ou idade
superior a 70 anos e mulheres se aposentam com 40 anos de trabalho ou idade superior
a 65 anos. */

#include <stdio.h>
int main (void)
{
    // vari�veis
    char genero;
    char feminino = {'F','f'};
    char masculino = {'M','m'};
    int idade, tempoTrabalho;

    //entrada de dados
    printf("genero, (f) feminino e (m) masculino: ");
    scanf("%c",&genero);

    printf("Informe sua idade: ");
    scanf("%d",&idade);

    printf("Informe o tempo de trabalho: ");
    scanf("%d",&tempoTrabalho);

    //processamento
    if (genero == feminino && tempoTrabalho >= 40 || idade > 65){
            printf("\nEsta apto a aposentadoria.\n");
        }
    else if (genero == masculino && tempoTrabalho >= 45 || idade > 70){
            printf("'\nEsta apto a aposentadoria.\n");
        }
        else{
            printf("\nGenero invalido.\n");
        }

    return 0;
}
