/* 5) Elaborar um programa que, dada a idade de um nadador, classifique-o em uma das
seguintes categorias:
Infantil A = 5 a 7 anos
Infantil B = 8 a 10 anos
Juvenil A = 11 a 13 anos
Juvenil B = 14 a 17 anos
S�nior = maiores de 17 anos
N�o categorizado como atleta = menor de 5 anos
Mas se o valor informado � negativo, informar ao usu�rio que o valor � inv�lido para o
escopo da solu��o. */

#include <stdio.h>
#include <stdlib.h>
int main (void)
{
    // vari�veis
    int idade;

    // entrada de dados
    printf("Informe uma idade: ");
    scanf("%d",&idade);

    // processamento e sa�da de dados
    switch (idade)
    {

        case 5:
        case 6:
        case 7:
            printf("Categoria: Infantil A.\n");
                break;

        case 8:
        case 9:
        case 10:
            printf("Categoria: Infantil B.\n");
                break;

        case 11:
        case 12:
        case 13:
            printf("Categoria: Juvenil A.\n");
                break;

        case 14:
        case 15:
        case 16:
        case 17:
            printf("Categoria: Juvenil B.\n");
                break;
    }

        if (idade > 17){
            printf("Categoria: Senior.\n");
        }
           if (0 <= idade && idade < 5){
            printf("Nao categorizado como atleta.\n");
           }
                else if (idade < 0){
                    printf("A idade eh invalida!\n");
                }

    return 0;
}
